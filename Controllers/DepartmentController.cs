﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUDMVC.Repository;
using CRUDMVC.Models;



namespace CRUDMVC.Controllers
{
    public class DepartmentController : Controller
    {
        DeptRepository DeptRepo = new DeptRepository();
       // private TestEntities db = new TestEntities();
        public ActionResult ListDepartment()
        {
            ModelState.Clear();
            return View(DeptRepo.ListDepartment());
        }
        //public JsonResult DeptNameExists(string username)
        //{
            
        //   // return Json(!Any(u => u.DeptName == DeptName), JsonRequestBehavior.AllowGet);
        //    //return Json(!String.Equals(username,  StringComparison.OrdinalIgnoreCase));
        //}
        //GET
        public ActionResult AddDepartment()
        {
            try
            {
                return View();
            }
            catch(Exception ex)
            {

                return View();
            }
            
        }
        [HttpPost]
        public ActionResult CheckDeptAvailability(DeptModel dmp)
        {
            DeptRepo.CheckDeptAvailability(dmp);
            return View();
        }
        [HttpPost]
        public ActionResult AddDepartment(DeptModel dmp)
        {
            try
            {
                DeptRepo.AddDepartment(dmp);
                ViewBag.SucessMessage = "Department is added successfully.";
                return View();
                //return RedirectToAction("ListDepartment");
            }
            catch (Exception ex) { return View(); }
        }
        public ActionResult EditDeptDetails(int id)
        {
            return View(DeptRepo.ListDepartment().Find(Dep => Dep.DeptID == id));
        }
        [HttpPost]
    public ActionResult EditDeptDetails(int id, DeptModel dept)
        {
            try
            {
                DeptRepo.UpdateDepartment(dept);
                return RedirectToAction("ListDepartment");
            }
            catch { return View(); }
        }
        public ActionResult DeleteDept(int id)
        {
            try
            {
                DeptRepo.DeleteDepartment(id);
                return RedirectToAction("ListDepartment");
            }
            catch { return View(); }
        }

    }
}