﻿using CRUDMVC.Models;
using CRUDMVC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRUDMVC.Controllers
{
    public class EmployeeController : Controller
    {
        EmpRepository EmpRepo = new EmpRepository();
        // GET: Employee/GetAllEmpDetails    
        public ActionResult GetAllEmpDetails()
        {
            //EmpRepository EmpRepo = new EmpRepository();
            ModelState.Clear();
            return View(EmpRepo.GetAllEmployees());
        }
        // get: employee/addemployee    
        public ActionResult AddEmployee()
        {
            return View();
        }

        // POST: Employee/AddEmployee    
        [HttpPost]
        public ActionResult AddEmployee(EmpModel emp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //      emprepository emprepo = new emprepository();

                    if (EmpRepo.AddEmployee(emp))
                    {
                        ViewBag.message = "employee details added successfully";
                    }
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/EditEmpDetails/5    
        public ActionResult EditEmpDetails(int id)
        {
           //EmpRepository EmpRepo = new EmpRepository();



            return View(EmpRepo.GetAllEmployees().Find(Emp => Emp.Empid == id));

        }

        // POST: Employee/EditEmpDetails/5    
        [HttpPost]

        public ActionResult EditEmpDetails(int id, EmpModel obj)
        {
            try
            {
                //       emprepository emprepo = new emprepository();

                EmpRepo.UpdateEmployee(obj);


                return RedirectToAction("GetAllEmpDetails");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        //GET: Employee/DeleteEmp/5    
        public ActionResult DeleteEmp(int id)
        {
            try
            {
                //EmpRepository EmpRepo = new EmpRepository();
                if (EmpRepo.DeleteEmployee(id))
                {
                    ViewBag.Message = "Employee details deleted successfully";

                }
                return RedirectToAction("GetAllEmpDetails");

            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
