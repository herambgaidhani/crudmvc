﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRUDMVC.Models;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
namespace CRUDMVC.Repository
{
    public class DeptRepository
    {
        private SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["getconn"].ToString();
            con = new SqlConnection(constr);
        }
      //  [System.Web.Services.WebMethod]
        public bool CheckDeptAvailability(DeptModel dep)
        {
            connection();
            SqlCommand cmd = new SqlCommand("CheckUserAvailability", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DeptName",dep.DeptName);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i >= 1) { return true; } else { return false; }
        }
        public bool AddDepartment(DeptModel dep)
        {
            connection();
            SqlCommand com = new SqlCommand("AddNewDeptDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@DeptName", dep.DeptName);
            com.Parameters.AddWithValue("@MGRName", dep.MGRName);
            com.Parameters.AddWithValue("@MGREMPNO", dep.MGREMPNO);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if(i>=1) {return true;} else {return false;}
        }
        public List<DeptModel> ListDepartment()
        {
            connection();
            List<DeptModel> DeptList = new List<DeptModel>();
            SqlCommand com = new SqlCommand("GetDepartments", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            foreach(DataRow dr in dt.Rows)
            {
                DeptList.Add(
                    new DeptModel
                    {
                        DeptID = Convert.ToInt32(dr["DeptID"]),
                        DeptName = Convert.ToString(dr["DeptName"]),
                        MGRName = Convert.ToString(dr["MGRName"]),
                        MGREMPNO = Convert.ToInt32(dr["MGREMPNO"])
                    }
                    );
            }
            return DeptList;
        }
        public bool UpdateDepartment(DeptModel dep)
        {
            connection();
            SqlCommand com = new SqlCommand("UpdateDeptDetails",con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@DeptID", dep.DeptID);
            com.Parameters.AddWithValue("@DeptName", dep.DeptName);
            com.Parameters.AddWithValue("@MGRName", dep.MGRName);
            com.Parameters.AddWithValue("@MGREMPNO", dep.MGREMPNO);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1) { return true; } else { return false; }
        }
        public bool DeleteDepartment(int id)
        {
            connection();
            SqlCommand com = new SqlCommand("DeleteDeptByID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@DeptID", id);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1) { return true; } else { return false; }
        }
    }
}