﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRUDMVC.Models
{
    public class DeptModel
    {
        [Display(Name = "Department ID")]
        public int DeptID { get; set; }

        [Display(Name = "Department Name")]
   //   [Required(ErrorMessage = "Department Name is required")]
       // [Remote("DeptNameExists", "Department", HttpMethod = "POST", ErrorMessage = "Department name is already exist.")]
        public string DeptName { get; set; }

        [Display(Name = "Manager Name")]
        [Required(ErrorMessage = " Manager Name is required")]
        public string MGRName { get; set; }

        [Display(Name = "Manager Employee ID")]
  //    [Required(ErrorMessage ="Manager Employee ID is required")]
        public int MGREMPNO { get; set; }
    }
}