USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateDeptDetails]    Script Date: 21-06-2021 16:08:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[UpdateDeptDetails]  
(  
   @DeptID int,
   @DeptName varchar (50),  
   @MGRName varchar (50),
   @MGREMPNO int
)  
as  
begin 
	update Department 
	set DeptName = @DeptName,
	MGRName = @MGRName,
	MGREMPNO = @MGREMPNO
	where DeptID = @DeptID
end
GO

