USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateEmpDetails]    Script Date: 21-06-2021 16:08:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[UpdateEmpDetails]  
(  
   @Empid int,  
   @Name varchar (50),  
   @City varchar (50),  
   @Address varchar (50)  
)  
as  
begin  
   Update Employee   
   set Name=@Name,  
   City=@City,  
   Address=@Address  
   where Empid=@Empid  
End 
GO

