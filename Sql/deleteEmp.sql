USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteEmpById]    Script Date: 21-06-2021 16:07:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[DeleteEmpById]  
(  
   @Empid int  
)  
as   
begin  
   Delete from Employee where Empid=@Empid  
End 
GO

