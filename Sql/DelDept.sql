USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteDeptById]    Script Date: 21-06-2021 16:06:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[DeleteDeptById]
(
	@DeptID int
)
as
begin
	delete from Department where DeptID = @DeptID
end
GO

