USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[AddNewEmpDetails]    Script Date: 21-06-2021 16:06:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[AddNewEmpDetails]  
(  
   @Name varchar (50),  
   @City varchar (50),  
   @Address varchar (50)  
)  
as  
begin  
   Insert into Employee values(@Name,@City,@Address)  
End 
GO

