USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[AddNewDeptDetails]    Script Date: 21-06-2021 16:05:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AddNewDeptDetails]  
(  
   @DeptName varchar (50),  
   @MGRName varchar (50),
   @MGREMPNO int
)  
as  
begin  
   Insert into Department values(@DeptName,@MGRName, @MGREMPNO)  
End 
GO

