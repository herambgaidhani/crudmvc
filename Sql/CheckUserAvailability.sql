USE [EmployeeData]
GO

/****** Object:  StoredProcedure [dbo].[CheckUserAvailability]    Script Date: 21-06-2021 16:06:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE PROCEDURE [dbo].[CheckUserAvailability]
 (
      @DeptName VARCHAR (50)
 )
AS
BEGIN
      SET NOCOUNT ON;
      IF NOT EXISTS(SELECT DeptName FROM Department
                    WHERE DeptName = @DeptName)
	  BEGIN
            SELECT 'TRUE'
      END
      ELSE
      BEGIN
            SELECT 'FALSE'
      END
END
GO

